/******************************* START IMPORT LIBRARIES *******************************/

const log = require("logger/logger"),
	config = require("config"),
	asyncLib = require("async"),
	Response = require("src/utils/response"),
	StatusCodes = require("src/utils/status_codes"),
	ResponseMessages = require("src/utils/response_messages"),
	Constants = require("src/utils/constants"),
	Utils = require("src/utils/utils");

/******************************* END IMPORT LIBRARIES *******************************/

const __MODEL__ = require("src/models/catalogue_model");

/******************************* MODEL IMPORT STATEMENT *******************************/

/******************************* START INTERFACE FUNCTIONS *******************************/

/**
 * Return paginates data format object
 * @param {number} pageSize
 * @param {number} totalRecordsCount
 * @param {number} currentPageNumber
 * @param {object []} currentPageData
 * @returns {object}
 */

const _returnPaginatedData = (
	pageSize,
	totalRecordsCount,
	currentPageNumber,
	currentPageData,
) => {
	const paginatedResponse = {
		current_page_number: currentPageNumber,
		total_records: totalRecordsCount,
		results: currentPageData,
	};

	if (!totalRecordsCount) {
		paginatedResponse.total_pages = 0;
	} else {
		if (totalRecordsCount % pageSize === 0) {
			paginatedResponse.total_pages = totalRecordsCount / pageSize;
		} else {
			paginatedResponse.total_pages =
				Math.floor(totalRecordsCount / pageSize) + 1;
		}
	}

	return paginatedResponse;
};

/**
 * Find many records matching with criteria
 * @param {object} condition
 * @param {object} projection
 * @returns {Promise}
 */
exports.find = (condition, projection) => {
	return new Promise(function(resolve, reject) {
		__MODEL__.find(condition, projection, function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Find ONE record matching with criteria
 * @param {object} condition
 * @param {object} projection
 * @returns {Promise}
 */
exports.findOne = (condition, projection) => {
	return new Promise(function(resolve, reject) {
		let projection_condition;
		if (!projection) {
			projection_condition = {};
		} else {
			projection_condition = projection;
		}

		__MODEL__.findOne(condition, projection_condition, function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Execute aggregation query on model
 * @param {object} aggregationQuery
 * @returns {Promise}
 */
exports.aggregate = aggregationQuery => {
	return new Promise(function(resolve, reject) {
		__MODEL__.aggregate(aggregationQuery, function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Fetch records matching to criteria in paginated format
 * @param {object} condition
 * @param {object} projection
 * @param {object {page_number: <number>, page_size: <number>}} paginationConfig
 * @returns {Promise}
 */
exports.findByPagination = (condition, projection, paginationConfig) => {
	return new Promise(function(resolve, reject) {
		const pageNumber = paginationConfig.page_number,
			pageSize = paginationConfig.page_size;

		asyncLib.waterfall(
			[
				function(totalCountCallback) {
					__MODEL__.count(condition, function(err, totalRowsCount) {
						if (err) {
							totalCountCallback(err);
						}

						totalCountCallback(null, totalRowsCount);
					});
				},
				function(totalCount, pageResultCallback) {
					if (!totalCount) {
						return pageResultCallback(
							null,
							_returnPaginatedData(pageSize, 0, pageNumber, []),
						);
					}

					let skipCount;

					if (pageNumber) {
						skipCount = (pageNumber - 1) * pageSize;
					} else {
						skipCount = pageNumber;
					}

					if (!projection) {
						projection_condition = {};
					} else {
						projection_condition = projection;
					}

					__MODEL__
						.find(condition, projection_condition)
						.sort({ _id: -1 })
						.skip(skipCount)
						.limit(pageSize)
						.exec(function(err, result) {
							if (err) {
								return pageResultCallback(err);
							}

							pageResultCallback(
								null,
								_returnPaginatedData(pageSize, totalCount, pageNumber, result),
							);
						});
				},
			],
			function(err, result) {
				if (err) {
					reject(err);
				}

				resolve(result);
			},
		);
	});
};

/**
 * Create single entry in database
 * @param {object} modelDataObject
 * @returns {Promise}
 */
exports.createOne = modelDataObject => {
	return new Promise(function(resolve, reject) {
		const modelObject = new __MODEL__();

		for (const key in modelDataObject) {
			modelObject[key] = modelDataObject[key];
		}

		modelObject.save(function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Create many entries in database by BULK insert.
 * Middleware is not execute in this case. Data is directly inserted into MongoDB as models are not inveolved.
 * @param {object []} modelDataObjects
 * @returns {Promise}
 */
exports.createMany = modelDataObjects => {
	return new Promise(function(resolve, reject) {
		__MODEL__.collection.insert(modelDataObjects, function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Create many entries in database but one after another with models involved
 * @param {object []} modelDataObjects
 * @returns {Promise}
 */
exports.createManyOneByOne = modelDataObjects => {
	return new Promise(function(resolve, reject) {
		const failedObjects = [],
			insertedObjects = [];

		asyncLib.eachSeries(
			modelDataObjects,
			function(singleObject, nextObjectCalback) {
				setImmediate(function() {
					exports
						.createOne(singleObject)
						.then(function(result) {
							insertedObjects.push({ err: null, data: result });
							nextObjectCalback(null);
						})
						.catch(function(error) {
							failedObjects.push({ err: error, data: singleObject });
							// proceed to next object without throwing error
							// try to insert as many objects as possible
							nextObjectCalback(null);
						});
				});
			},
			function(err) {
				if (err) {
					reject(err);
				}

				resolve({
					inserted_data: insertedObjects,
					failed_inserts: failedObjects,
				});
			},
		);
	});
};

/**
 * Update multiple entries which match criteria
 * @param {object} condition
 * @param {object} updateFields
 * @returns {Promise}
 */
exports.update = (condition, updateFields) => {
	return new Promise(function(resolve, reject) {
		__MODEL__.update(condition, updateFields, { multi: true }, function(
			err,
			result,
		) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Update SINGLE entry which match criteria
 * @param {object} condition
 * @param {object} updateFields
 * @returns {Promise}
 */
exports.updateOne = (condition, updateFields) => {
	return new Promise(function(resolve, reject) {
		__MODEL__.updateOne(condition, updateFields, function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Remove multiple entries matching with criteria
 * @param {object} condition
 * @returns {Promise}
 */
exports.delete = condition => {
	return new Promise(function(resolve, reject) {
		__MODEL__.remove(condition, function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/**
 * Remove SINGLE entry matching with criteria
 * @param {object} condition
 * @returns {Promise}
 */
exports.deleteOne = condition => {
	return new Promise(function(resolve, reject) {
		__MODEL__.deleteOne(condition, function(err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

/******************************* END INTERFACE FUNCTIONS *******************************/
