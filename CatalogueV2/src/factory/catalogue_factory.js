/******************************* START IMPORT LIBRARIES *******************************/

const log = require("logger/logger"),
	config = require("config"),
	Response = require("src/utils/response"),
	StatusCodes = require("src/utils/status_codes"),
	ResponseMessages = require("src/utils/response_messages"),
	Constants = require("src/utils/constants"),
	Utils = require("src/utils/utils"),
	EndPoints = require("src/utils/end_points"),
	catalogueInterface = require("src/interfaces/catalogue_interface");

/******************************* END IMPORT LIBRARIES *******************************/

/******************************* START FACTORY FUNCTIONS *******************************/

/**
 *
 * @param request
 * @param reply
 * @private
 */
exports.getListCatalogue = async (request, h) => {
	let response_data = null;
	const condition = { };

	try {
		response_data = await catalogueInterface.find(condition, {});
		return h
			.response(
				Response.sendResponse(
					true,
					response_data,
					ResponseMessages.SUCCESS,
					StatusCodes.OK,
				),
			)
			.code(StatusCodes.OK);
	} catch (err) {
		return h
			.response(
				Response.sendResponse(
					false,
					err,
					ResponseMessages.ERROR,
					StatusCodes.BAD_REQUEST,
				),
			)
			.code(StatusCodes.BAD_REQUEST);
	}
};

/**
 *
 * @param request
 * @param reply
 * @private
 */
exports.getOneCatalogue = async (request, h) => {
	let response_data = null;
	const condition = {
		_id: request.params.id,
	};

	try {
		response_data = await catalogueInterface.findOne(condition, {});
		return h
			.response(
				Response.sendResponse(
					true,
					response_data,
					ResponseMessages.SUCCESS,
					StatusCodes.OK,
				),
			)
			.code(StatusCodes.OK);
	} catch (err) {
		return h
			.response(
				Response.sendResponse(
					false,
					err,
					ResponseMessages.ERROR,
					StatusCodes.BAD_REQUEST,
				),
			)
			.code(StatusCodes.BAD_REQUEST);
	}
};

/**
 *
 * @param request
 * @param reply
 * @private
 */
exports.getByPaginationCatalogue = async (request, h) => {
	let response_data = null;
	const condition = {};

	try {
		response_data = await catalogueInterface.findByPagination(
			condition,
			{},
			request.query,
		);
		return h
			.response(
				Response.sendResponse(
					true,
					response_data,
					ResponseMessages.SUCCESS,
					StatusCodes.OK,
				),
			)
			.code(StatusCodes.OK);
	} catch (err) {
		return h
			.response(
				Response.sendResponse(
					false,
					err,
					ResponseMessages.ERROR,
					StatusCodes.BAD_REQUEST,
				),
			)
			.code(StatusCodes.BAD_REQUEST);
	}
};

/**
 *
 * @param request
 * @param reply
 * @private
 */
exports.createOneCatalogue = async (request, h) => {
	let response_data = null;
	try {
		response_data = await catalogueInterface.createOne(request.payload);
		return h
			.response(
				Response.sendResponse(
					true,
					response_data,
					ResponseMessages.SUCCESS,
					StatusCodes.OK,
				),
			)
			.code(StatusCodes.OK);
	} catch (err) {
		return h
			.response(
				Response.sendResponse(
					false,
					err,
					ResponseMessages.ERROR,
					StatusCodes.BAD_REQUEST,
				),
			)
			.code(StatusCodes.BAD_REQUEST);
	}
};

/**
 *
 * @param request
 * @param reply
 * @private
 */
exports.createMany = async (request, h) => {
	let response_data = null;

	try {
		response_data = await catalogueInterface.createMany(request.payload);
		return h
			.response(
				Response.sendResponse(
					true,
					response_data,
					ResponseMessages.SUCCESS,
					StatusCodes.OK,
				),
			)
			.code(StatusCodes.OK);
	} catch (err) {
		return h
			.response(
				Response.sendResponse(
					false,
					err,
					ResponseMessages.ERROR,
					StatusCodes.BAD_REQUEST,
				),
			)
			.code(StatusCodes.BAD_REQUEST);
	}
};

/**
 *
 * @param request
 * @param reply
 * @private
 */
exports.updateOneCatalogue = async (request, h) => {
	let response_data = null;
	const condition = {
		_id: request.params.id,
	};

	try {
		response_data = await catalogueInterface.updateOne(
			condition,
			request.payload,
		);
		return h
			.response(
				Response.sendResponse(
					true,
					response_data,
					ResponseMessages.SUCCESS,
					StatusCodes.OK,
				),
			)
			.code(StatusCodes.OK);
	} catch (err) {
		return h
			.response(
				Response.sendResponse(
					false,
					err,
					ResponseMessages.ERROR,
					StatusCodes.BAD_REQUEST,
				),
			)
			.code(StatusCodes.BAD_REQUEST);
	}
};

/**
 *
 * @param request
 * @param reply
 * @private
 */
exports.deleteOneCatalogue = async (request, h) => {
	let response_data = null;
	const condition = {
		_id: request.params.id,
	};

	try {
		response_data = await catalogueInterface.deleteOne(
			condition,
			request.payload,
		);
		return h
			.response(
				Response.sendResponse(
					true,
					response_data,
					ResponseMessages.SUCCESS,
					StatusCodes.OK,
				),
			)
			.code(StatusCodes.OK);
	} catch (err) {
		return h
			.response(
				Response.sendResponse(
					false,
					err,
					ResponseMessages.ERROR,
					StatusCodes.BAD_REQUEST,
				),
			)
			.code(StatusCodes.BAD_REQUEST);
	}
};

/******************************* END FACTORY FUNCTIONS *******************************/
