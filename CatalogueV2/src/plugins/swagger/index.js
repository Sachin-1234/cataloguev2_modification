const log = require("logger/logger"),
	config = require("config"),
	{ resolve } = require("path");

const register = async server => {
	try {
		return server.register([
			require("@hapi/inert"),
			require("@hapi/vision"),
			{
				plugin: require("hapi-swagger"),
				options: {
					info: {
						title: "CatalogueV2",
						description: "CatalogueV2 Api Documentation",
						version: "1.0",
						termsOfService:
							"This documentation is private to iauro systems pvt ltd",
						contact: {
							name: "iauro Admin",
							email: "admin@iauro.com",
						},
					},
					host: `pioneer.iauro.run:${config.server.port}`,
					//basePath: `/1588077633003-cataloguev2-microservice-${config.env}`,
					schemes: ["http", "https"],
					tags: [
						{
							name: "CatalogueV2",
							description: "Api interface.",
						},
					],
					grouping: "tags",
					templates: resolve("templates", "swagger"),
				},
			},
		]);
	} catch (err) {
		log.info(`Error registering swagger plugin: ${err}`);
	}
};

module.exports = {
	register,
	info: { name: "Swagger Documentation", version: "1.0.0" },
};
