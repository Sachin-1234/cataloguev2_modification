const mongoose = require("mongoose"),
	Schema = mongoose.Schema;

const catalogueRootSchema = new Schema({
	name: {
		type: Schema.Types.String,
		default: "",
	},
	summary: {
		type: Schema.Types.String,
		default: "",
	},
	version: {
		type: Schema.Types.String,
		default: "",
	},
	type: {
		type: Schema.Types.String,
		default: "",
	},
	technology: {
		type: Schema.Types.String,
		default: "",
	},
	card_image: {
		type: Schema.Types.String,
		default: "",
	},
	created_at: {
		type: Schema.Types.String,
		default: "",
	},
	updated_at: {
		type: Schema.Types.String,
		default: "",
	},
	author: {
		type: Schema.Types.String,
		default: "",
	},
	source_url: {
		type: Schema.Types.String,
		default: "",
	},
	deployment_info: {
		type: Schema.Types.String,
		default: "",
	},
	configuration_info: {
		type: Schema.Types.String,
		default: "",
	},
	swagger_document: {
		type: Schema.Types.String,
		default: "",
	},
	tags: {
		type: [Schema.Types.String],
		default: [],
	},
});

module.exports = mongoose.model("catalogue", catalogueRootSchema, "catalogue");
