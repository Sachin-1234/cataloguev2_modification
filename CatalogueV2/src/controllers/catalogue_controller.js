/*
 * catalogue Controller File
 */

const Response = require("src/utils/response"),
	Validation = require("src/validations/catalogue_validations"),
	catalogueFactory = require("src/factory/catalogue_factory"),
	Joi = require("@hapi/joi");

const response_format = Joi.object({
		is_success: Joi.boolean().required(),
		result: Joi.any().optional(),
		message: Joi.string().required(),
		status_code: Joi.number().required(),
	}),
	response = {
		status: {
			200: response_format,
			201: response_format,
			400: response_format,
			500: response_format,
		},
	};

exports.createOneCatalogue = {
	description: "Create a new record of  catalogue",
	notes: "Create a new record of  catalogue",
	tags: ["api"],
	plugins: {
		"hapi-swaggered": {
			responses: Response.responses,
		},
	},
	validate: Validation.create_one_catalogue,
	//auth: 'simple',
	handler: (request, h) => {
		return catalogueFactory.createOneCatalogue(request, h);
	},
	response,
};

exports.updateOneCatalogue = {
	description: "Update a single record of  catalogue",
	notes: "Update a single record of  catalogue",
	tags: ["api"],
	plugins: {
		"hapi-swaggered": {
			responses: Response.responses,
		},
	},
	validate: Validation.update_one_catalogue,
	//auth: 'simple',
	handler: (request, h) => {
		return catalogueFactory.updateOneCatalogue(request, h);
	},
	response,
};

exports.getOneCatalogue = {
	description: "Get single record of  catalogue",
	notes: "Get single record of  catalogue",
	tags: ["api"],
	plugins: {
		"hapi-swaggered": {
			responses: Response.responses,
		},
	},
	validate: Validation.get_one_catalogue,
	//auth: 'simple',
	handler: (request, h) => {
		return catalogueFactory.getOneCatalogue(request, h);
	},
	response,
};

exports.getListCatalogue = {
	description: "Get the list of  catalogue",
	notes: "Get the list of  catalogue",
	tags: ["api"],
	plugins: {
		"hapi-swaggered": {
			responses: Response.responses,
		},
	},
	validate: Validation.get_list_catalogue,
	//auth: 'simple',
	handler: (request, h) => {
		return catalogueFactory.getListCatalogue(request, h);
	},
	response,
};

exports.getByPaginationCatalogue = {
	description: "Get the list by pagination of  catalogue",
	notes: "Get the list by pagination of  catalogue",
	tags: ["api"],
	plugins: {
		"hapi-swaggered": {
			responses: Response.responses,
		},
	},
	validate: Validation.get_by_pagination_catalogue,
	//auth: 'simple',
	handler: (request, h) => {
		return catalogueFactory.getByPaginationCatalogue(request, h);
	},
	response,
};

exports.deleteOneCatalogue = {
	description: "Delete a single record of  catalogue",
	notes: "Delete a single record of  catalogue",
	tags: ["api"],
	plugins: {
		"hapi-swaggered": {
			responses: Response.responses,
		},
	},
	validate: Validation.delete_one_catalogue,
	//auth: 'simple',
	handler: (request, h) => {
		return catalogueFactory.deleteOneCatalogue(request, h);
	},
	response,
};
