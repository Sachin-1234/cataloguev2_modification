/*
 * catalogue Validations File
 */

const Joi = require("@hapi/joi");

module.exports = Object.freeze({
	create_one_catalogue: {
		payload: {
			name: Joi.string(),
			summary: Joi.string(),
			version: Joi.string(),
			type: Joi.string(),
			technology: Joi.string(),
			card_image: Joi.string(),
			created_at: Joi.string(),
			updated_at: Joi.string(),
			author: Joi.string(),
			source_url: Joi.string(),
			deployment_info: Joi.string(),
			configuration_info: Joi.string(),
			swagger_document: Joi.string(),
			tags: Joi.array().items(Joi.string()),
		},
		headers: Joi.object({
			authorization: Joi.string().required(),
		}).options({ allowUnknown: true }),
	},
	update_one_catalogue: {
		params: {
			id: Joi.string().required(),
		},
		payload: {
			name: Joi.string(),
			summary: Joi.string(),
			version: Joi.string(),
			type: Joi.string(),
			technology: Joi.string(),
			card_image: Joi.string(),
			created_at: Joi.string(),
			updated_at: Joi.string(),
			author: Joi.string(),
			source_url: Joi.string(),
			deployment_info: Joi.string(),
			configuration_info: Joi.string(),
			swagger_document: Joi.string(),
			tags: Joi.array().items(Joi.string()),
		},
		headers: Joi.object({
			authorization: Joi.string().required(),
		}).options({ allowUnknown: true }),
	},
	get_one_catalogue: {
		params: {
			id: Joi.string().required(),
		},
		headers: Joi.object({
			authorization: Joi.string().required(),
		}).options({ allowUnknown: true }),
	},
	get_list_catalogue: {
		headers: Joi.object({
			authorization: Joi.string().required(),
		}).options({ allowUnknown: true }),
	},
	get_by_pagination_catalogue: {
		headers: Joi.object({
			authorization: Joi.string().required(),
		}).options({ allowUnknown: true }),
	},
	delete_one_catalogue: {
		params: {
			id: Joi.string().required(),
		},
		headers: Joi.object({
			authorization: Joi.string().required(),
		}).options({ allowUnknown: true }),
	},
	
});
