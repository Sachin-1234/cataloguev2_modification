/*
 * catalogue Route File
 */

const catalogueController = require("src/controllers/catalogue_controller");

module.exports = [
	{
		method: "POST",
		path: "/catalogues",
		config: catalogueController.createOneCatalogue,
	},
	{
		method: "PUT",
		path: "/catalogues/{id}",
		config: catalogueController.updateOneCatalogue,
	},
	{
		method: "GET",
		path: "/catalogues/{id}",
		config: catalogueController.getOneCatalogue,
	},
	{
		method: "GET",
		path: "/catalogues/list",
		config: catalogueController.getListCatalogue,
	},
	{
		method: "GET",
		path: "/catalogues/list/pagination",
		config: catalogueController.getByPaginationCatalogue,
	},
	
	{
		method: "DELETE",
		path: "/catalogues/{id}",
		config: catalogueController.deleteOneCatalogue,
	},
];
