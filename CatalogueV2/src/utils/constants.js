module.exports = Object.freeze({
	CONTENT_TYPE: {
		JSON: "application/json",
		FORM: "application/x-www-form-urlencoded",
		FORM_DATA: "multipart/form-data",
	},
	METHODS: {
		GET: "GET",
		POST: "POST",
		PUT: "PUT",
		DELETE: "DELETE",
	},
});
