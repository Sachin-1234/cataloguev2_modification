/******************************* START IMPORT LIBRARIES *******************************/
process.env.NODE_ENV = "test";
const path = require("path");
require("app-module-path").addPath(path.join(__dirname, "../"));
const chai = require("chai"),
	chaiHttp = require("chai-http"),
	should = chai.should(),
	qs = require("qs"),
	server = require("../server");

const mongo = require("../databases/mongodb");

chai.use(chaiHttp);
/******************************* END IMPORT LIBRARIES *******************************/

/******************************* START TEST FUNCTIONS *******************************/

describe("catalogue", () => {
	let result;
	let mongo_result, _id;
	before(async () => {
		// Before block
		try {
			global.sequelizeModels = null;
			result = await server.init();
			mongo_result = await mongo.init();
			console.log("Server Started :");
			return result;
		} catch (e) {
			console.log("Error in starting server : ", e);
			return e;
		}
	});

	describe("POST /catalogues", () => {
		it("POST /catalogues", done => {
			let options = {
				method: "POST",
				url: `/catalogues`,
				payload: {
					name: "",
					summary: "",
					version: "",
					type: "",
					technology: "",
					card_image: "",
					created_at: "",
					updated_at: "",
					author: "",
					source_url: "",
					deployment_info: "",
					configuration_info: "",
					swagger_document: "",
					tags: [""],
				},
				headers: {
					authorization: "Basic token",
				},
			};
			chai
				.request("http://localhost:23231")
				.post(options.url)
				.set(options.headers)
				.send(options.payload)
				.end((err, res) => {
					_id = JSON.parse(res.text).result._id;
					res.should.have.status(200);
					done();
				});
		});
	});
	describe("PUT /catalogues/{id}", () => {
		it("PUT /catalogues/{id}", done => {
			let options = {
				method: "PUT",
				url: `/catalogues/{id}`.replace(`{ id }`.replace(/ /g, ""), `${_id}`),
				payload: {
					name: "",
					summary: "",
					version: "",
					type: "",
					technology: "",
					card_image: "",
					created_at: "",
					updated_at: "",
					author: "",
					source_url: "",
					deployment_info: "",
					configuration_info: "",
					swagger_document: "",
					tags: [""],
				},
				headers: {
					authorization: "Basic token",
				},
			};
			chai
				.request("http://localhost:23231")
				.put(options.url)
				.set(options.headers)
				.send(options.payload)
				.end((err, res) => {
					_id = JSON.parse(res.text).result._id;
					res.should.have.status(200);
					done();
				});
		});
	});
	describe("GET /catalogues/{id}", () => {
		it("GET /catalogues/{id}", done => {
			let options = {
				method: "GET",
				url: `/catalogues/{id}`.replace(`{ id }`.replace(/ /g, ""), `${_id}`),
				headers: {
					authorization: "Basic token",
				},
			};
			chai
				.request("http://localhost:23231")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});
	describe("GET /catalogues/list", () => {
		it("GET /catalogues/list", done => {
			let options = {
				method: "GET",
				url: `/catalogues/list`,
				headers: {
					authorization: "Basic token",
				},
			};
			chai
				.request("http://localhost:23231")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});
	describe("GET /catalogues/list/pagination", () => {
		it("GET /catalogues/list/pagination", done => {
			let options = {
				method: "GET",
				url: `/catalogues/list/pagination`,
				headers: {
					authorization: "Basic token",
				},
			};
			chai
				.request("http://localhost:23231")
				.get(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});
	describe("DELETE /catalogues/{id}", () => {
		it("DELETE /catalogues/{id}", done => {
			let options = {
				method: "DELETE",
				url: `/catalogues/{id}`.replace(`{ id }`.replace(/ /g, ""), `${_id}`),
				headers: {
					authorization: "Basic token",
				},
			};
			chai
				.request("http://localhost:23231")
				.delete(options.url)
				.set(options.headers)
				.end((err, res) => {
					res.should.have.status(200);
					done();
				});
		});
	});

	after(async () => {
		// Before block
		try {
			let mongo_close = await mongo.close();
			result.stop();
		} catch (e) {
			console.log("Error in starting server : ", e);
			return e;
		}
	});
});

/******************************* END TEST FUNCTIONS *******************************/
